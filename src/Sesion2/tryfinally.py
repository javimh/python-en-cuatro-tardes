#Try finally ejemplo

a = int(input("Introduce primer divisor: "))
b = int(input("Introduce segundo divisor: "))

try:
    print(a/b)

except:
    print("no puedes dividir por cero")
    
finally:
    print("esto se ejecuta siempre")

print("Bye bye")
