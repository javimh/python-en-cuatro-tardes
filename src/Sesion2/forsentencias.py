#Example of loop statements
#Show odd numbers and stop in 13
for n in range(1,20): #i iterate over 1,2,...,20
    if(n%2==0):
        continue
    if(n==13):
        pass
    print(n)
