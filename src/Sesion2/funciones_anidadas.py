#Ejemplo de funciones anidadas

def division(num,denom):
    #la funcion zero hereda las variables que hemos pasado
    def zero():
        if denom!=0 :
            return True
    if(zero()):
        return num/denom

#Creamos una funcion a partir de otra
def funcion_mitad(num):
    return division(num,2)

    
num = int(input("Introduce el numerador: "))
denom = int(input("Introduce el denominador: "))
print(division(num,denom))
print("La mitad es: ")
print(funcion_mitad(num))
